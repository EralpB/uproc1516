// ###############################################################
// ######################### INCLUDES ############################
// ###############################################################
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <sys/time.h>
#include <time.h>

#define BILLION 1000000000L


// ###############################################################
// ########################## TYPES ##############################
// ###############################################################
struct cacheline {
	void* next;
	uint64_t   index;
	uint64_t   space[6];
};

typedef struct cacheline cacheline;

// ################################################################
// ######################## METHODS ###############################
// ################################################################

static inline
int gettime(void) {
  // to be implemented
struct timeval tv;
gettimeofday(&tv,0);
int elapsed = (tv.tv_sec)*1000000 + tv.tv_usec;
return elapsed;
}

static inline 
long gettime_nanoseconds(void){
struct timespec ts;
clock_gettime(CLOCK_REALTIME, &ts);
long elapsed = (ts.tv_sec*BILLION+ts.tv_nsec);
return elapsed;
}


int isRelativelyPrime (int a, int b) { // Assumes a, b > 0
  for ( ; ; ) {
    if (!(a %= b)) return b == 1 ;
    if (!(b %= a)) return a == 1 ;
  }
}

void fillArray(int size, int stride,  cacheline* array)
{
	int i = 0;

	for(i = 0; i<size; i++)
	{
		(*(array+i)).index = i;
		(*(array+i)).next = (void*)(array+((i+stride)%size));
	}
}

int run(int size, cacheline* array)
{
	int i;
	long start = 0;
	long stop = 0;
	long res = 0;
	int wasVisited = 0;

	cacheline cur = *array;

	// Avoid cold start and check for element 0
	for(i = 0; i<size; i++)
	{
		if(cur.index == 0)
		{
			if(!wasVisited)
				wasVisited = 1;
			else
			{
				printf("Fault on next %i\n", i);
				return -1;
			}
		}
		cur = *((cacheline*)cur.next);
	}
        

	// This already is the first access to the array
	start = gettime_nanoseconds();
	cur = *array;
	stop = gettime_nanoseconds();

	res += stop - start;

	for(i = 0; i<999999; i++)
	{
		start = gettime_nanoseconds();
		cur = *((cacheline*)cur.next);
		stop = gettime_nanoseconds();

		res += (stop - start);

		// Prevent compiler optimization
		//printf("index: %i\n", (int)(cur.index));
	}

	return res;
}

// -------------------------- main -------------------------------
int main(int argc, char* argv[])
{
	int n;
	int stride = 4160;
	int maxSize = 81920; // For now this value means 5MB, which is exactly 81920*64B
        cacheline* array = malloc(maxSize*sizeof(cacheline));
	for(n = 1; n<maxSize; n = n+2)
	{
                if(!isRelativelyPrime(n, stride)){
                        printf("Not Coprime skipping %i\n", n);
                        continue;
                }

		int res = 0;
		int time = 0;
		long tickspersec = 2400000000;
		//cacheline* array = malloc(n*sizeof(cacheline));
		fillArray(n, stride, array);
		res = run(n, array);
		if(res == -1)
			printf("Run with size %i failed\n", n);
		else {
			// Compute and print CPI
			printf("%i %f\n",n, ((res*1.0/BILLION)*tickspersec)/1000000);
		}
		

		//free(array);
	}
        free(array);
	return 0;
}
