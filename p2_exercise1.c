#include <stdlib.h>
#include <stdio.h>
#include <sys/time.h>
#include <math.h>
#include <time.h>

#define ARR_LENGTH 1024*4096
#define TICKS_PER_SEC 2400000000
#define RUNS_PER_STRIDE 10

#define BILLION 1000000000L

static inline
double gettime(void) {
struct timeval tv;
gettimeofday(&tv,0);
double elapsed = (tv.tv_sec)*1000000 + tv.tv_usec;
return elapsed;
}

static inline
long gettime_nanoseconds(void){
    struct timespec ts;
    clock_gettime(CLOCK_REALTIME, &ts);
    long elapsed = (ts.tv_sec*BILLION+ts.tv_nsec);
    return elapsed;
}

void fillByteArray(char* arr, int length){
    int i = 0;
    for(i=0;i<length;i++){
        arr[i] = 1;
    }
}

double calculateAverageCPIforStride(int stride,char* byteArray){
    long start,end,totalTime;
    int accessCount;
    long sum;
    int i;

    sum=0;
    totalTime=0;
    accessCount=0;
    
    for(i=0;i<ARR_LENGTH;i+=stride){
        accessCount++;
        start = gettime_nanoseconds();
        sum += byteArray[i];
        end = gettime_nanoseconds();
        totalTime += (end-start);
    }
    printf("SUM:%ld\n",sum);
    return ((totalTime*1.0/BILLION)*TICKS_PER_SEC)*1.0/accessCount;
}

int cmpfunc (const void * a, const void * b)
{
   return ( *(double*)a - *(double*)b );
}

double AvgWithoutOutliers(double* arr, int size){
    double sum;
    int i, cutoff;
    
    cutoff = (int)ceil(size/20); //cutoff 40% of data, 20% from bottom 20% from top
    qsort(arr, size, sizeof(double), cmpfunc);
    sum = 0;
    for(i=cutoff;i<size-cutoff;i++){
        sum += arr[i];
    }
    
    return sum/(size-cutoff*2);
}

int main(int argc, char **argv)
{
    int stride, strideRun;
    double* strideResults = malloc(sizeof(double)*RUNS_PER_STRIDE);
    char* byteArray = malloc(sizeof(char)*ARR_LENGTH);
    fillByteArray(byteArray,ARR_LENGTH);
    
    for(stride=1;stride<4096;stride++){
        for(strideRun=0;strideRun<RUNS_PER_STRIDE;strideRun++){
            strideResults[strideRun] = calculateAverageCPIforStride(stride, byteArray);
        }
        printf("%d %f\n",stride, AvgWithoutOutliers(strideResults, RUNS_PER_STRIDE));
    }
    
    free(strideResults);
    free(byteArray);
    return 0;
}
