#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <sys/time.h>
#include "options.h"
//#include <mmintrin.h>
//#include <emmintrin.h>

int debug = 0;
double *results;
double *ratios;
unsigned long   *sizes;

int no_sz = 1, no_ratio =1, no_version=1;




static inline
double gettime(void) {
  // to be implemented
struct timeval tv;
gettimeofday(&tv,0);
double elapsed = (tv.tv_sec)*1000000 + tv.tv_usec;
return elapsed;
}


static void toupper_simple(char * text) {
  // to be implemented
  int i = 0;
  int text_len = strlen(text);
  for(i=0;i<text_len;i++){
      if(text[i]>= 'a' && text[i] <= 'z'){
          text[i] -= 0x20;
      }
  }
}

/*
void print_128(__uint128_t n){
if(n == 0){printf("\n");return;}
print_128(n/10);
printf("%c",n%10+0x30);
}

static void toupper_optimised_sse(char* text) {
    int current_index, phase_length;
    int nth, nth_byte;
    __m128i* current_data;

    __m128i lowercase_boundry;
    __m128i null_characters;
    __m128i constant_difference;
    __m128i input;
    __m128i isuppercase;
    __m128i to_subtract;
    __m128i result;
    __m128i is_null_character;
 
    __uint128_t result_as_integer = 0;
    __uint128_t lowercase_boundry_as_128bit_integer = 0;
    __uint128_t constant_difference_as_128bit_integer = 0;
    __uint128_t tmp_input = 0; 
    __uint128_t is_null_character_result = 0;

    __uint64_t lowercase_boundry_as_64bit_integer = 0;
    __uint64_t constant_difference_as_64bit_integer = 0;

    //These can be hard-coded because they are static
    lowercase_boundry_as_64bit_integer = 6944656592455360608; //8 times 'a'-1 characters 
    constant_difference_as_64bit_integer = 2314885530818453536; //8 times 0x20 bytes

    null_characters = _mm_setzero_si128();
    lowercase_boundry = _mm_set_epi64x (lowercase_boundry_as_64bit_integer, lowercase_boundry_as_64bit_integer);
    constant_difference = _mm_set_epi64x (constant_difference_as_64bit_integer, constant_difference_as_64bit_integer); 
    current_index = 0;
    phase_length = 16;
    while(1){
        
        current_data = (__m128i*)(text+current_index);
        input = _mm_loadu_si128(current_data);
        is_null_character = _mm_cmpeq_epi8(input, null_characters);
        is_null_character_result = (__uint128_t)is_null_character; 
        
        if(is_null_character_result != 0){
         for(nth=15;nth>=0;nth--){
         nth_byte = (is_null_character_result >> (nth*8)) & 0xFF;
         if(nth_byte == 0xFF){
             phase_length=15-nth;
             if(phase_length==0){
                 return;
             }
             break;
         }
         } 
        }
 
        isuppercase = _mm_cmpgt_epi8 (input, lowercase_boundry);
        to_subtract = _mm_and_si128 (isuppercase, constant_difference);    
  
        result = _mm_sub_epi8 (input, to_subtract);
        _mm_stream_si128(text+current_index, result);
        current_index += phase_length;
     }
    return;
}
*/

static void toupper_arm(char* text)
{
	asm(
	// Load counter
	"MOV r0, #0\n\t"

	// Nullpointer check
	"CMP %[pointer], #0\n\t"

	"LOOP:"
	// Load next char
	"LDRB r1, [%[pointer], r0]\n\t"

	// Check for end of string
	"CMP r1, #0\n\t"
	"BEQ END\n\t"

	// Range check
	"CMP r1, #97\n\t"
	"SUBGE r1, #0x20\n\t"

	// Write back and increase loop counter
	"STRB r1, [%[pointer], r0]\n\t"
	"ADD r0, #1\n\t"
	"B LOOP\n\t"

	"END:"
	:
	: [pointer] "r" (text)
	:
	);
}

/*****************************************************************/


// align at 16byte boundaries
void* mymalloc(unsigned long int size)
{
     void* addr = malloc(size+32);
     return (void*)((unsigned long int)addr /16*16+16);
}

char createChar(int ratio){
	char isLower = rand()%100;

	// upper case=0, lower case=1
	if(isLower < ratio)
		isLower =0;
	else
		isLower = 1;

	char letter = rand()%26+1; // a,A=1; b,B=2; ...

	return 0x40 + isLower*0x20 + letter;

}

char * init(unsigned long int sz, int ratio){
    int i=0;
    char *text = (char *) mymalloc(sz+1);
    srand(1);// ensures that all strings are identical
    for(i=0;i<sz;i++){
			char c = createChar(ratio);
			text[i]=c;
	  }
    text[i] = '\0';
    return text;
}



/*
 * ******************* Run the different versions **************
 */

typedef void (*toupperfunc)(char *text);

void run_toupper(int size, int ratio, int version, toupperfunc f, const char* name)
{
   double start, stop;
		int index;

		index =  ratio;
		index += size*no_ratio;
		index += version*no_sz*no_ratio;

    char *text = init(sizes[size], ratios[ratio]);


    if(debug) printf("Before: %.40s...\n",text);

    start = gettime();
    (*f)(text);
    stop = gettime();
    results[index] = stop-start;

    if(debug) printf("After:  %.40s...\n",text);
}

struct _toupperversion {
    const char* name;
    toupperfunc func;
} toupperversion[] = {
    { "simple", toupper_simple},
    { "arm",    toupper_arm },
    { 0,0 }
};


void run(int size, int ratio)
{
	int v;
	for(v=0; toupperversion[v].func !=0; v++) {
		run_toupper(size, ratio, v, toupperversion[v].func, toupperversion[v].name);
	}

}

void printresults(){
	int i,j,k,index;
	printf("%s\n", OPTS);

	for(j=0;j<no_sz;j++){
		for(k=0;k<no_ratio;k++){
			printf("Size: %ld \tRatio: %f \tRunning time:", sizes[j], ratios[k]);
			for(i=0;i<no_version;i++){
				index =  k;
				index += j*no_ratio;
				index += i*no_sz*no_ratio;
				printf("\t%s: %f", toupperversion[i].name, results[index]);
			}
			printf("\n");
		}
	}
}

int main(int argc, char* argv[])
{ 

    unsigned long int min_sz=80000, max_sz = 0, step_sz = 10000;
		int min_ratio=50, max_ratio = 0, step_ratio = 1;
		int arg,i,j,v;
		int no_exp;

		for(arg = 1;arg<argc;arg++){
			if(0==strcmp("-d",argv[arg])){
				debug = 1;
			}
			if(0==strcmp("-l",argv[arg])){
					min_sz = atoi(argv[arg+1]);
					if(arg+2>=argc) break;
					if(0==strcmp("-r",argv[arg+2])) break;
					if(0==strcmp("-d",argv[arg+2])) break;
					max_sz = atoi(argv[arg+2]);
					step_sz = atoi(argv[arg+3]);
			}
			if(0==strcmp("-r",argv[arg])){
					min_ratio = atoi(argv[arg+1]);
					if(arg+2>=argc) break;
					if(0==strcmp("-l",argv[arg+2])) break;
					if(0==strcmp("-d",argv[arg+2])) break;
					max_ratio = atoi(argv[arg+2]);
					step_ratio = atoi(argv[arg+3]);
			}

		}
    for(v=0; toupperversion[v].func !=0; v++)
		no_version=v+1;
		if(0==max_sz)  no_sz =1;
		else no_sz = (max_sz-min_sz)/step_sz+1;
		if(0==max_ratio)  no_ratio =1;
		else no_ratio = (max_ratio-min_ratio)/step_ratio+1;
		no_exp = v*no_sz*no_ratio;
		results = (double *)malloc(sizeof(double[no_exp]));
		ratios = (double *)malloc(sizeof(double[no_ratio]));
		sizes = (long *)malloc(sizeof(long[no_sz]));

		for(i=0;i<no_sz;i++)
			sizes[i] = min_sz + i*step_sz;
		for(i=0;i<no_ratio;i++)
			ratios[i] = min_ratio + i*step_ratio;

		for(i=0;i<no_sz;i++)
			for(j=0;j<no_ratio;j++)
				run(i,j);

		printresults();
    return 0;
}
