import matplotlib.pyplot as plt
from scipy.ndimage.filters import gaussian_filter1d
from scipy.interpolate import interp1d
import numpy as np

x = []
y = []

cacheline = 64

with open('p2_exercise2_output2.txt','r') as f:
    for line in f.read().splitlines():
        if "Coprime" in line:
            continue
        data_point = line.split(' ')
        x.append(int(data_point[0])*cacheline*1.0/(1024))
        y.append(float(data_point[1]))

gaussian_y = gaussian_filter1d(y, sigma=50)
f = interp1d(x, gaussian_y)

plt.plot(x, f(x), '-')
plt.ylabel('Average CPI for each access')
plt.xlabel('Size of array in kilobytes')
plt.title('Graph for Caches Exercise #2')
plt.annotate('32kb',xytext=(0.1, 0.1), textcoords='axes fraction', xy=(32,f(32)), arrowprops=dict(facecolor='black',alpha=0.4))
plt.annotate('256kb',xytext=(0.3, 0.1), textcoords='axes fraction', xy=(256,f(256)), arrowprops=dict(facecolor='black', alpha=0.4))
plt.annotate('3mb',xytext=(0.6, 0.1), textcoords='axes fraction', xy=(3000,f(3000)), arrowprops=dict(facecolor='black', alpha=0.4))


plt.figure()
plt.plot(x[:500], f(x[:500]), '-')
plt.ylabel('Average CPI for each access')
plt.xlabel('Size of array in kilobytes')
plt.title('Zoomed graph for L1 Cache')

plt.figure()
plt.plot(x, y, '-')
plt.title('Raw data for Exercise #2 w/o interpolation and gaussian filter')
plt.ylabel('Average CPI for each access')
plt.xlabel('Size of array in kilobytes')

x = []
y = []
with open('p2_exercise1_output.txt','r') as f:
    for line in f.read().splitlines():
        if "SUM" in line:
            continue
        data_point = line.split(' ')
        x.append(int(data_point[0]))
        y.append(float(data_point[1]))

gaussian_y = gaussian_filter1d(y, sigma=20)
f = interp1d(x, gaussian_y)

plt.figure()
plt.plot(x, f(x), '-')
plt.ylabel('Average CPI for each access')
plt.xlabel('Stride')
plt.title('Graph for Exercise#1 with interpolation and gaussian filter')

plt.figure()
plt.plot(x[:100], f(x[:100]), '-')
plt.ylabel('Average CPI for each access')
plt.xlabel('Stride')
plt.title('Zoomed graph for cacheline Ex#1')
plt.annotate('64',xytext=(0.6, 0.9), textcoords='axes fraction', xy=(64,f(64)), arrowprops=dict(facecolor='black',alpha=0.4))

plt.figure()
plt.plot(x, y, '-')
plt.title('Raw data for Exercise #1 w/o interpolation and gaussian filter')
plt.ylabel('Average CPI for each access')
plt.xlabel('Stride')

plt.show()
